## Libraries used
1. **React - a frontend framework**
2. **Redux - global state management**
3. **Redux-saga - middleware for redux to handle side effects, i.e. call API. Easier to test and Better at handling failures**
4. **Styled-components - managed the CSS styles scoped to a single react components and not leak to any other elements in the page**
5. **Google-map-react - to show location of users on detail page**
6. **Typescript - superset of Javascript to add static typing**
7. **Jest - for testing**

## 🚀 Quick start

### 1. `yarn install`

Installs all dependencies.

### 2. **Prepare .env file**

    ```shell
    # create a .env.development file in root directory
    GOOGLE_MAP_API_KEY=<The API KEY OF GOOGLE MAP>
    ```

### 3. `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!