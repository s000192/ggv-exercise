import { takeEvery } from 'redux-saga/effects';
import { watchFetchRequest, handleFetch } from '../../../store/postList/sagas'
import { PostlistActionTypes } from '../../../store/postList/types';

describe('watchFetchRequest', () => {
  const genObject = watchFetchRequest();
  
  it('should wait for every FETCH_REQUEST action and call handleFetch', () => {
    expect(genObject.next().value)
      .toEqual(takeEvery(PostlistActionTypes.FETCH_REQUEST, handleFetch));
  });
});

