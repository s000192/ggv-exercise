import { toggleComments, fetchPostsRequest, fetchSuccess, fetchError } from '../../../store/postList/actions'
import { action } from 'typesafe-actions'
import { PostlistActionTypes } from '../../../store/postList/types'

describe("Post List Actions",()=>{
    it("should toggle comments successfully",()=>{
        expect(toggleComments(0)).toEqual(action(PostlistActionTypes.TOGGLE_COMMENTS, {postId: 0}))
    })

    it("should request to fetch posts",()=>{
        expect(fetchPostsRequest()).toEqual(action(PostlistActionTypes.FETCH_REQUEST))
    })

    it("should fetch posts successfully",()=>{
        expect(fetchSuccess([])).toEqual(action(PostlistActionTypes.FETCH_SUCCESS, []))
    })

    it("should report error if fetching is failed",()=>{
        expect(fetchError("error")).toEqual(action(PostlistActionTypes.FETCH_ERROR, "error"))
    })
})