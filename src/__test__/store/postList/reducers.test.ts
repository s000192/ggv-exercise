import { postListReducer } from '../../../store/postList/reducers'
import { PostListState } from '../../../store/postList/types'
import { toggleComments, fetchPostsRequest, fetchSuccess, fetchError } from '../../../store/postList/actions'
import { Post } from '../../../store/common/types';

describe('Post List Reducer', () => {
    let initialState: PostListState;

    beforeEach(() => {
        initialState = {
            data: [],
            errors: undefined,
            loading: false
        };
    })

    it("should request to fetch posts", () => {
        const finalState = postListReducer(initialState, fetchPostsRequest())
        expect(finalState).toEqual({
            data: [],
            errors: undefined,
            loading: true
        });
    });

    it("should have fetched posts successfully", () => {
        const samplePosts: Post[] = [
            {
                "userId": 1,
                "id": 1,
                "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
                "commentsToggled": false
            },
            {
                "userId": 1,
                "id": 2,
                "title": "qui est esse",
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
                "commentsToggled": false
            }
        ]
        const finalState = postListReducer(initialState, fetchSuccess(samplePosts))
        expect(finalState).toEqual({
            data: samplePosts,
            errors: undefined,
            loading: false
        });
    });

    it("should report error if fetching is failed", () => {
        const finalState = postListReducer(initialState, fetchError("error"))
        expect(finalState).toEqual({
            data: [],
            errors: "error",
            loading: false
        });
    });

    it("should toggle comments",()=>{
        initialState = {
            data: [
                {
                    "userId": 1,
                    "id": 1,
                    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
                    "commentsToggled": false
                },
                {
                    "userId": 1,
                    "id": 2,
                    "title": "qui est esse",
                    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
                    "commentsToggled": false
                }
            ],
            errors: undefined,
            loading: false
        };
        const finalState = postListReducer( initialState, toggleComments(1))
        expect(finalState).toEqual({
            data: [
                {
                    "userId": 1,
                    "id": 1,
                    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
                    "commentsToggled": true
                },
                {
                    "userId": 1,
                    "id": 2,
                    "title": "qui est esse",
                    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
                    "commentsToggled": false
                }
            ],
            errors: undefined,
            loading: false
        });
    });
});