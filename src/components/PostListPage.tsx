import React from "react";
import styled from "styled-components";
import { Post, User, Comment } from "../store/common/types";
import { fetchPostsRequest, toggleComments } from "../store/postList/actions";
import { ApplicationState } from "../store";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchUsersRequest } from "../store/userList/actions";
import CommentsSection from "./CommentsSection";

interface PropsFromState {
    posts: {
        loading: boolean
        data: Post[]
        errors?: string
    }
    users: {
        loading: boolean
        data: User[]
        errors?: string
    }
    comments: {
        loading: boolean
        data: Comment[]
        errors?: string
    }
}

interface PropsFromDispatch {
    fetchPostsRequest: typeof fetchPostsRequest
    fetchUsersRequest: typeof fetchUsersRequest
    toggleComments: typeof toggleComments
}

export type AllProps = PropsFromState & PropsFromDispatch

const HeaderStyled = styled.header`
  text-align: center;
  background-color: #FFFFFF;
  padding: 10px;
  font-weight: 800;
  font-size: 5vw;
`

const PostStyled = styled.div`
    display: flex;
    flex-direction: column;
    margin: 2vw;
    background-color: #FFFFFF;
    border-color: #DFDFDF;
    border: 2px solid;
    padding: 20px;
`

const PostTitleStyled = styled.div`
    padding: 5%;
    font-size: 5vw;
    font-weight: 800;
`

const PostBodyStyled = styled.div`
    padding: 5%;
    font-size: 3vw;
    font-weight: 300;
`

const PostCreatorStyled = styled(Link)`
    padding: 0 5%;
    font-size: 3vw;
    font-weight: 300;
`

const CommentButtonStyled = styled.button`
    margin: 0 5%;
    font-size: 3vw;
`

export class PostListPage extends React.Component<AllProps> {
    componentDidMount() {
        const { fetchPostsRequest: fpr } = this.props
        const { fetchUsersRequest: fur } = this.props
        fpr()
        fur()
    }

    private renderData() {
        const { /* loading, */ posts, users } = this.props

        // const [commentsToggled, setCommentsToggled] = useState(false)

        const handleClick = (postId: number) => {
            // setCommentsToggled(!commentsToggled)
            this.props.toggleComments(postId)
        }

        return (
            <>
                {posts.data.slice(0, 10).map((post: Post) => (
                    <PostStyled>
                        <PostTitleStyled>{`${post.title}`}</PostTitleStyled>
                        <PostCreatorStyled to={`/detail/${post.userId}`}>{`${users.data.filter(user => user.id === post.userId)[0].email}`}</PostCreatorStyled>
                        <PostBodyStyled>{`${post.body}`}</PostBodyStyled>
                        <CommentButtonStyled onClick={() => handleClick(post.id)}>{post.commentsToggled? "Hide Comments" : "Show Comments"}</CommentButtonStyled>
                        {post.commentsToggled? <CommentsSection key={post.id} postId={post.id} /> : null}
                    </PostStyled>
                ))}
            </>
        )
    }

    public render() {
        return (
            <>
                <HeaderStyled>First 10 posts</HeaderStyled>
                {this.renderData()}
            </>
        )
    }
}

const mapStateToProps = ({ postList, userList, commentList }: ApplicationState) => ({
    posts: {
        loading: postList.loading,
        errors: postList.errors,
        data: postList.data
    },
    users: {
        loading: userList.loading,
        errors: userList.errors,
        data: userList.data
    },
    comments: {
        loading: commentList.loading,
        errors: commentList.errors,
        data: commentList.data
    }
})

const mapDispatchToProps = {
    fetchPostsRequest,
    fetchUsersRequest,
    toggleComments: (postId: number) => toggleComments(postId)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostListPage)