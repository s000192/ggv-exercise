import React from "react"
import styled from "styled-components"
import { useHistory } from "react-router-dom";
import { User } from "../store/common/types"
import { ApplicationState } from "../store"
import { connect } from "react-redux"
import Map from "./Map"

interface PropsFromState {
    loading: boolean
    data: User[]
    errors?: string
    match: {
        params: {
            id: string
        }
    }
}


const HeaderStyled = styled.header`
    display: flex;
    text-align: center;
    background-color: #FFFFFF;
    padding: 10px;
    font-weight: 800;
    font-size: 5vw;

    p {
        text-align: center;
    }
`

const UserStyled = styled.div`
    display: flex;
    margin: 2vw;
    background-color: #FFFFFF;
    border-color: #DFDFDF;
    border: 2px solid;
    padding: 20px;
`

const UserNameStyled = styled.div`
    width: 80%;
    padding: 5%;
    font-size: 5vw;
    font-weight: 800;
`

const ButtonStyled = styled.button`
    color: #EC6A6B;
    border: none;
    background-color: #FFFFFF;
    font-size: 50px;
`

export const BackButton = () => {
    let history = useHistory();
    return (
        <>
            <ButtonStyled onClick={() => history.goBack()}>{"<"}</ButtonStyled>
        </>
    );
};

class UserDetailPage extends React.Component<PropsFromState> {

    public render() {
        const detail = this.props.data.filter((item) => item.id === parseInt(this.props.match.params.id))[0]
        return (
            <>
                <HeaderStyled>
                    <BackButton />
                    <p>Your User</p>
                </HeaderStyled>
                <Map lat={detail.address.geo? parseFloat(detail.address.geo.lat) : 11.0168} lng={detail.address.geo? parseFloat(detail.address.geo.lng) : 76.9558} />
                <UserStyled>
                    {/* <UserImageStyled src={detail.picture} alt="" /> */}
                    <UserNameStyled>{`${detail.name}`}</UserNameStyled>
                    <p>{process.env.API_KEY}</p>
                </UserStyled>
            </>
        )
    }
}

const mapStateToProps = ({ userList }: ApplicationState) => ({
    loading: userList.loading,
    errors: userList.errors,
    data: userList.data
})


export default connect(
    mapStateToProps
)(UserDetailPage)