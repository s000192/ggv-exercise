import React from "react";
import styled from "styled-components";
import { Comment } from "../store/common/types";
import { ApplicationState } from "../store";
import { fetchCommentsRequest } from "../store/commentList/actions";
import { connect } from "react-redux";

interface PropsFromState {
    comments: {
        loading: boolean
        data: Comment[]
        errors?: string
    }
    postId: number
}

interface PropsFromDispatch {
    fetchCommentsRequest: typeof fetchCommentsRequest
}

type AllProps = PropsFromState & PropsFromDispatch

const CommentsStyled = styled.div`
    padding: 5%;
    font-size: 3vw;
    font-weight: 300;
`

const CommentsEmailStyled = styled.div`
    padding: 0 5%;
    font-size: 3vw;
    font-weight: 300;
`

export class CommentsSection extends React.Component<AllProps> {
    componentDidMount() {
        const { fetchCommentsRequest: fcr, postId } = this.props
        fcr(postId)
    }

    private renderData() {
        const { /* loading, */ comments, postId } = this.props

        return (
            <>
                {comments.data.filter((comment) => comment.postId === postId ).map((comment) => (
                    <>
                        <CommentsStyled>{comment.body}</CommentsStyled>
                        <CommentsEmailStyled>{comment.email}</CommentsEmailStyled>
                    </>
                ))}
            </>
        )
    }

    public render() {
        return (
            <>
                {this.renderData()}
            </>
        )
    }
}

const mapStateToProps = ({ commentList }: ApplicationState) => ({
    comments: {
        loading: commentList.loading,
        errors: commentList.errors,
        data: commentList.data
    }
})

const mapDispatchToProps = {
    fetchCommentsRequest
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CommentsSection)