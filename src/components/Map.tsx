import React, { useState } from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from './Marker';

const GOOGLE_MAP_API_KEY: string = process.env.GOOGLE_MAP_API_KEY!

interface MapProps {
  lat: number,
  lng: number
}

const Map = (props: MapProps) => {
  const { lat, lng } = props;

  const getMapOptions = () => {
    return {
      disableDefaultUI: true,
      mapTypeControl: true,
      streetViewControl: true,
      styles: [{ featureType: 'poi', elementType: 'labels', stylers: [{ visibility: 'on' }] }],
    };
  };

  const [center] = useState({ lat: lat? lat : 11.0168, lng: lng? lng : 76.9558 });
  const [zoom] = useState(11);
  return (
    <div style={{ height: '100vh', width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_MAP_API_KEY }}
        defaultCenter={center}
        defaultZoom={zoom}
        options={getMapOptions}
      >
        <Marker
          lat={lat? lat : 11.0168}
          lng={lng? lng : 76.9558}
          name="My Marker"
          color="blue"
        />
      </GoogleMapReact>
    </div>
  );
}

export default Map;