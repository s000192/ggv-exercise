import axios from "axios"

export async function getPostList() {
    return await axios.get("https://jsonplaceholder.typicode.com/posts")
}

export async function getUserList() {
    return await axios.get("https://jsonplaceholder.typicode.com/users")
}

export async function getComments(postId: number) {
    return await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
}