import { Store, createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
import { composeWithDevTools } from 'redux-devtools-extension'
import { History } from 'history'
import { rootReducer, ApplicationState, rootSaga } from './store/index'
import { loadState } from './localStorage'

export default function configureStore(
    history: History,
): Store<ApplicationState> {
    const composeEnhancers = composeWithDevTools({})
    const sagaMiddleware = createSagaMiddleware()

    const persistedState = loadState();

    const store = createStore(
        rootReducer,
        persistedState,
        composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
    )

    sagaMiddleware.run(rootSaga)
    return store
}