import { combineReducers /*, Dispatch, Reducer, Action, AnyAction */ } from 'redux'
import { RouterState, connectRouter } from 'connected-react-router'
import { all, fork } from 'redux-saga/effects'
import { createBrowserHistory } from 'history'
import { PostListState } from './postList/types'
import { postListReducer } from './postList/reducers'
import { postListSaga } from './postList/sagas'
import { UserListState } from './userList/types'
import { userListReducer } from './userList/reducer'
import { userListSaga } from './userList/sagas'
import { CommentListState } from './commentList/types'
import { commentListReducer } from './commentList/reducers'
import { commentListSaga } from './commentList/sagas'

export const history = createBrowserHistory()

export interface ApplicationState {
    postList: PostListState,
    userList: UserListState,
    commentList: CommentListState,
    router: RouterState
}

export const rootReducer = combineReducers<ApplicationState>({
    postList: postListReducer,
    userList: userListReducer,
    commentList: commentListReducer,
    router: connectRouter(history)
})

export function* rootSaga() {
    yield all([
        fork(postListSaga),
        fork(userListSaga),
        fork(commentListSaga)
    ])
}