import { action } from 'typesafe-actions'
import { CommentlistActionTypes } from './types'
import { Comment } from '../common/types'

export const fetchCommentsRequest = (postId: number) => action(CommentlistActionTypes.FETCH_REQUEST, {postId: postId})
export const fetchSuccess = (data: Comment[]) => action(CommentlistActionTypes.FETCH_SUCCESS, data)
export const fetchError = (message: string) => action(CommentlistActionTypes.FETCH_ERROR, message)