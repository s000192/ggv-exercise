import { Comment } from "../common/types";

export type ApiResponse = Record<string, any>

export enum CommentlistActionTypes {
    FETCH_REQUEST = '@@commentlist/FETCH_REQUEST',
    FETCH_SUCCESS = '@@commentlist/FETCH_SUCCESS',
    FETCH_ERROR = '@@commentlist/FETCH_ERROR',
    SELECTED = '@@commentlist/SELECTED'
}

export interface CommentListState {
    readonly loading: boolean
    readonly data: Comment[]
    readonly errors?: string
}