import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { CommentlistActionTypes } from './types'
import { fetchError, fetchSuccess, fetchCommentsRequest } from './actions'
import { getComments } from '../../utils/callApi'

function* handleFetch(action: ReturnType<typeof fetchCommentsRequest>) {
    try {
        const res = yield call(getComments, action.payload.postId)

        if (res.error) {
            yield put(fetchError(res.error))
        } else {
            yield put(fetchSuccess(res))
        }
    } catch (err) {
        if (err instanceof Error) {
            yield put(fetchError(err.stack!))
        } else {
            yield put(fetchError('An unknown error occured.'))
        }
    }
}

function* watchFetchRequest() {
    yield takeEvery(CommentlistActionTypes.FETCH_REQUEST, handleFetch)
}


export function* commentListSaga() {
    yield all([fork(watchFetchRequest)])
}