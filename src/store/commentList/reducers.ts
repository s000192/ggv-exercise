import { Reducer } from 'redux'
import { CommentListState, CommentlistActionTypes } from './types'
import { Comment } from "../common/types";

const initialState: CommentListState = {
    data: [],
    errors: undefined,
    loading: false
}

const reducer: Reducer<CommentListState> = (state = initialState, action) => {
    switch (action.type) {
      case CommentlistActionTypes.FETCH_REQUEST: {
        return { ...state, loading: true, postId: action.payload }
      }
      case CommentlistActionTypes.FETCH_SUCCESS: {
        const newCommentList = [ ...state.data ]
        action.payload.data.filter((comment: Comment) => newCommentList.find(item => item.id === comment.id)? null : newCommentList.push(comment))
        return { ...state, loading: false, data: newCommentList }
      }
      case CommentlistActionTypes.FETCH_ERROR: {
        return { ...state, loading: false, errors: action.payload }
      }
      default: {
        return state
      }
    }
  }

export { reducer as commentListReducer }