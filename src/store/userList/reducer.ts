// import { UserListAction } from "./actions";
// import { FetchUserListAction } from "./sagas";
import { Reducer } from 'redux'
import { UserListState, UserlistActionTypes } from './types'

const initialState: UserListState = {
    data: [],
    errors: undefined,
    loading: false
}

const reducer: Reducer<UserListState> = (state = initialState, action) => {
    switch (action.type) {
      case UserlistActionTypes.FETCH_REQUEST: {
        return { ...state, loading: true }
      }
      case UserlistActionTypes.FETCH_SUCCESS: {
        return { ...state, loading: false, data: action.payload.data }
      }
      case UserlistActionTypes.FETCH_ERROR: {
        return { ...state, loading: false, errors: action.payload }
      }
      default: {
        return state
      }
    }
  }

export { reducer as userListReducer }