import { User } from "../common/types";

export type ApiResponse = Record<string, any>

export enum UserlistActionTypes {
    FETCH_REQUEST = '@@userlist/FETCH_REQUEST',
    FETCH_SUCCESS = '@@userlist/FETCH_SUCCESS',
    FETCH_ERROR = '@@userlist/FETCH_ERROR',
    SELECTED = '@@userlist/SELECTED'
}

export interface UserListState {
    readonly loading: boolean
    readonly data: User[]
    readonly errors?: string
}