import { action } from 'typesafe-actions'
import { UserlistActionTypes } from './types'
import { User } from '../common/types'

export const fetchUsersRequest = () => action(UserlistActionTypes.FETCH_REQUEST)
export const fetchSuccess = (data: User[]) => action(UserlistActionTypes.FETCH_SUCCESS, data)
export const fetchError = (message: string) => action(UserlistActionTypes.FETCH_ERROR, message)