import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { UserlistActionTypes } from './types'
import { fetchError, fetchSuccess } from './actions'
import { getUserList } from '../../utils/callApi'

function* handleFetch() {
    try {
        const res = yield call(getUserList)

        if (res.error) {
            yield put(fetchError(res.error))
        } else {
            yield put(fetchSuccess(res))
        }
    } catch (err) {
        if (err instanceof Error) {
            yield put(fetchError(err.stack!))
        } else {
            yield put(fetchError('An unknown error occured.'))
        }
    }
}

function* watchFetchRequest() {
    yield takeEvery(UserlistActionTypes.FETCH_REQUEST, handleFetch)
}


export function* userListSaga() {
    yield all([fork(watchFetchRequest)])
}