import { Post } from "../common/types";

export type ApiResponse = Record<string, any>

export enum PostlistActionTypes {
    FETCH_REQUEST = '@@postlist/FETCH_REQUEST',
    FETCH_SUCCESS = '@@postlist/FETCH_SUCCESS',
    FETCH_ERROR = '@@postlist/FETCH_ERROR',
    SELECTED = '@@postlist/SELECTED',
    TOGGLE_COMMENTS = '@@postlist/TOGGLE_COMMENTS'
}

export interface PostListState {
    readonly loading: boolean
    readonly data: Post[]
    readonly errors?: string
}