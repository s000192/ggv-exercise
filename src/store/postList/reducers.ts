import { Reducer } from 'redux'
import { PostListState, PostlistActionTypes } from './types'
import { Post } from '../common/types'

const initialState: PostListState = {
  data: [],
  errors: undefined,
  loading: false
}

const reducer: Reducer<PostListState> = (state = initialState, action) => {
  switch (action.type) {
    case PostlistActionTypes.FETCH_REQUEST: {
      return { ...state, loading: true }
    }
    case PostlistActionTypes.FETCH_SUCCESS: {
      return {
        ...state, loading: false, data: action.payload.map((post: Post) => {
          const add = { commentsToggled: false }
          return { ...post, ...add }
        })
      }
    }
    case PostlistActionTypes.FETCH_ERROR: {
      return { ...state, loading: false, errors: action.payload }
    }
    case PostlistActionTypes.TOGGLE_COMMENTS: {
      const { postId } = action.payload
      const newPostList = state.data.slice()
      newPostList.map((post) => post.id === postId ? post.commentsToggled = !post.commentsToggled : null)
      return { ...state, data: newPostList }
    }
    default: {
      return state
    }
  }
}

export { reducer as postListReducer }