import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { PostlistActionTypes } from './types'
import { fetchError, fetchSuccess } from './actions'
import { getPostList } from '../../utils/callApi'

export function* handleFetch() {
    try {
        const res = yield call(getPostList)

        if (res.error) {
            yield put(fetchError(res.error))
        } else {
            yield put(fetchSuccess(res))
        }
    } catch (err) {
        if (err instanceof Error) {
            yield put(fetchError(err.stack!))
        } else {
            yield put(fetchError('An unknown error occured.'))
        }
    }
}

export function* watchFetchRequest() {
    yield takeEvery(PostlistActionTypes.FETCH_REQUEST, handleFetch)
}


export function* postListSaga() {
    yield all([fork(watchFetchRequest)])
}