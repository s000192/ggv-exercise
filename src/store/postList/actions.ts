import { action } from 'typesafe-actions'
import { PostlistActionTypes } from './types'
import { Post } from '../common/types'

export const toggleComments = (postId: number) => action(PostlistActionTypes.TOGGLE_COMMENTS, {postId: postId})
export const fetchPostsRequest = () => action(PostlistActionTypes.FETCH_REQUEST)
export const fetchSuccess = (data: Post[]) => action(PostlistActionTypes.FETCH_SUCCESS, data)
export const fetchError = (message: string) => action(PostlistActionTypes.FETCH_ERROR, message)