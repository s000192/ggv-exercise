import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import * as serviceWorker from './serviceWorker';
import configureStore from './configureStore'
import { Route, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router';
import { history } from './store/index'
import UserDetailPage from './components/UserDetailPage';
import { saveState } from './localStorage';
import { throttle } from 'lodash';
import PostListPage from './components/PostListPage';

const store = configureStore(history)

store.subscribe(throttle(() => {
  saveState({
    postList: store.getState().postList,
    userList: store.getState().userList,
    commentList: store.getState().commentList,
    router: store.getState().router
  });
}, 1000));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div>
          <Switch>
            <Route path="/" exact={true} component={PostListPage} />
            <Route path="/detail/:id" exact={true} component={UserDetailPage} />
          </Switch>
        </div>
      </ConnectedRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
